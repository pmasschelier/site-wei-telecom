var overlay = {
    // (A) INITIALIZE - CREATE OVERLAY HTML
    ewrap: null,    // html wrapper
    econtent: null, // html contents
    init: () => {
        overlay.ewrap = document.createElement("div");
        overlay.ewrap.id = "owrap";
        overlay.ewrap.innerHTML = `<div id="oclose" onclick="overlay.hide()">X</div>
    <div id="ocontent"></div>`;
        overlay.econtent = overlay.ewrap.querySelector("#ocontent");
        document.body.appendChild(overlay.ewrap);
    },

    // (B) SHOW OVERLAY
    show: (content) => {
        overlay.econtent.innerHTML = content;
        overlay.ewrap.classList.add("show");
    },

    // (C) HIDE OVERLAY
    hide: () => {
        overlay.ewrap.classList.remove("show");
    },

    // (D) LOAD & SHOW CONTENT VIA AJAX
    load: (url, data) => {
        // (D1) FORM DATA
        let form = new FormData();
        if (data) {
            for (let [k, v] of Object.entries(data)) {
                form.append(k, v);
            }
        }

        // (D2) SET & SHOW CONTENTS
        fetch(url, {method: "post", body: form})
            .then((res) => {
                return res.text();
            })
            .then((txt) => {
                overlay.show(txt);
            });
    }
};

// (E) ATTACH OVERLAY TO PAGE
document.addEventListener("DOMContentLoaded", overlay.init);

function overlay_show(overlay_src) {
    overlay.show(`<img src='${overlay_src}' id='trombi-big' />`)
    setTimeout(() => {
        document.getElementById("owrap").addEventListener("click", () => overlay.hide());
    }, 300);
}

setInterval(() => {
    try {
        const e = document.getElementById("trombi-big");
        const x = e.naturalWidth;
        const y = e.naturalHeight;
        const ratio = y / x;
        const w = window.innerWidth;
        const h = window.innerHeight;
        if (w * ratio> h) {
            e.style.height = h + "px";
            e.style.width = "auto";
        } else {
            e.style.width = w + "px";
            e.style.height = "auto";
        }
    } catch {
        // nope
    }
}, 100);
