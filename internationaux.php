<!DOCTYPE HTML>
<html>
<head>
    <title>WEI - Télécom Paris</title>
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport"/>

    <!-- Stylesheets -->
    <noscript>
        <link href="assets/css/noscript.css" rel="stylesheet"/>
    </noscript>
    <link href="assets/css/main.css" rel="stylesheet"/>
    <link href="style.css" rel="stylesheet"/>
    <link href="carousel.css" rel="stylesheet"/>
    <link href="mobile.css" rel="stylesheet"/>

    <style>
        #image-internationaux {
            width: 100%;
            height: 500px;
            background: url('images/Internationaux4.jpg') no-repeat center center;
            background-size: cover;
            border-top-left-radius: 30px;
            border-top-right-radius: 30px;
        }
    </style>

    <script>

        function resize_image_internationaux() {
            let ratio = 752 / 1656;
            $("#image-internationaux").height($("#image-internationaux").parent().width() * ratio)
        }

        window.onload = function () {

            resize_image_internationaux()
            $(window).resize(resize_image_internationaux);

        }
    </script>
</head>

<!-- Wrapper -->
<div id="wrapper">

    <section class="wrapper floating-section" id="accueil">
        <br/>
        <a href="index.php<?php if($_GET['lang'] != NULL) echo '?lang='.$_GET['lang'] ?>"><h3 class="welcome-text"><span
                style="border: 1px solid black; padding: 5px; border-radius: 5px;"><?php echo ($_GET['lang'] == 'en' ? 'Home page' : 'Retour à la page d\'accueil') ?></span>
        </h3></a>
        <h1 class="welcome-text"><?php echo ($_GET['lang'] == 'en' ? 'Information for the international students' : 'Informations pour les étudiants internationaux') ?></h1>

    </section>


    <!-- One -->
    <section class="floating-section wrapper style2 spotlights" id="one">
        <section id="texte1" style="flex-direction: column;">
            <div id="image-internationaux"></div>
            <br/>
            <div class="content" style="width: 100%">
                <div class="inner" style="width: 100%">
                    <div class="more longText" style="width: 100%">
						<?php include 'assets/texts/' . ($_GET['lang'] == 'en' ? 'en' : 'fr') . '/internationals.html'; ?>
                    </div>

                </div>
            </div>
        </section>
    </section>
</div>


<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.scrollex.min.js"></script>
<script src="assets/js/jquery.scrolly.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>


</body>
</html>