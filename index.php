<?php
$en = ($_GET['lang'] == 'en');

include_once 'showmore.php';
?>
<!DOCTYPE HTML>
<!--
	Hyperspace by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>WEI - Télécom Paris</title>
		<meta charset="utf-8"/>
		<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport"/>

		<style>
			/* loading animation */
			@keyframes fadeout {
				0% {
					opacity: 1
				}
				50% {
					opacity: 0
				}
				100% {
					opacity: 0
				}
			}

			#loading {
				width: 100%;
				height: 100%;
				display: flex;
				text-align: center;
				flex-direction: column;
				position: absolute;
				top: 0;
				left: 0;
				z-index: 999999;
				<?php
				/* l'idée là c'est que le logo de preloading est inclus dans la page ;
				comme ça il ne peut pas mettre de temps supplémentaire à charger, il s'affiche direct.
				Pour désactiver : true -> false
				*/
				if(true) // embedded logo
				{
				?>
                background: rgba(124, 189, 238, 1) url("data:image/svg+xml,<?php include "images/logo.svg.urlencoded" ?>") no-repeat center center;
				<?php
				}
				else // logo dans un dossier
				{
				?>
                background: rgba(124, 189, 238, 1) url("images/logo.svg") no-repeat center center;
				<?php
				}
				?>
				background-size: contain;
			}

			.loaded {
				animation: fadeout 4s ease-out alternate;
			}

			body {
				overflow: hidden;
			}

			.flag
			{
			width: 50px;
			height: 50px;
			}

		</style>


	</head>
	<body class="is-preload">


	<div id="loading">
	</div>

	<!-- Sidebar -->
	<section id="sidebar">



		<div class="inner">
			<nav>
				<ul>
					<li><img id="logo" src="images/logo.png"/></li>
					<li><a href="#accueil"><?php echo $en ? 'What is the WEI?': 'Qu’est-ce que le WEI ?'; ?></a></li>
					<li><a href="#one"><?php echo $en ? 'More infos': 'Plus d\'infos'; ?></a></li>
					<li><a href="#two"><?php echo $en ? 'Integration week': 'Semaine d\'intégration'; ?></a></li>
					<li><a href="#three"><?php echo $en ? 'Internationals': 'Internationaux'; ?></a></li>
					<li><a href="#four">Contact</a></li>
					<li><a href="#five">FAQ</a></li>
					<li><a href="#six"><?php echo $en ? 'Ticketing': 'Billetterie'; ?></a></li>
				</ul>
			</nav>
		</div>
		</section>

		<!-- Wrapper -->
		<div id="wrapper">

			<!-- Intro -->
			<section class="wrapper fullscreen fade-up" id="intro">
				<h1 class="welcome-text">
				<?php if ($en)
					echo 'Welcome to Telecom Paris';
				else
					echo 'Bienvenue à Télécom Paris';
				?>
				<br/>
				<a href="?lang=fr"><img src="images/fr.png" class="flag"/></a>
				<a href="?lang=en"><img src="images/en.png" class="flag"/></a>
				</h1>

			</section>
			<section class="wrapper fade-up floating-section" id="accueil">
				<h1 class="welcome-text">
				<?php if ($en)
					echo 'What is the WEI';
				else
					echo 'Qu’est-ce que le WEI ?';
				?>
				</h1>

				<section class="extra-padding">
					<div class="content">
						<div class="inner">
							<div class="more longText">
								<p>
									<?php include 'assets/texts/' . ($en ? 'en' : 'fr') . '/welcome.html'; ?>
								</p>
							</div>
						</div>
					</div>
				</section>


			</section>


			<section class="floating-section wrapper fade-up" >
				<h1 class="welcome-text"><?php echo $en ? 'The WEI in pictures': 'Le WEI en photos'; ?></h1>

				<section id="carousel-section">
					<div class="content">
						<div class="inner">
							<div class="more longText">
								<div id="carousel-preload" style="display: none">
								</div>
								<div id="carousel-container">
									<!-- Basé sur https://codepen.io/aybukeceylan/pen/RwrRPoO -->
										<div class="carousel">
											<input checked id="item-1" name="slider" type="radio">
											<input id="item-2" name="slider" type="radio">
											<input id="item-3" name="slider" type="radio">
											<div class="cards">
												<label class="card" id="song-1">
													<img class="carousel-img" alt="song"
														src="images/defile/35.JPG">
												</label>
												<label class="card" id="song-2">
													<img class="carousel-img" alt="song"
														src="images/defile/43.JPG" >
												</label>
												<label class="card"  id="song-3">
													<img class="carousel-img" alt="song"
														src="images/defile/55.JPG" >
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>

					<section id="videos-container">
						<div class="content">
							<div class="inner">
								<div class="more longText" id="videos-inner-container">
									<iframe allowfullscreen src="https://www.youtube.com/embed/sdRpbIpdHNE"></iframe>
									<iframe allowfullscreen src="https://www.youtube.com/embed/S59tE6X-VUE"></iframe>
								</div>
							</div>
						</div>
					</section>
				</section>


			<!-- One -->

			<section class="floating-section wrapper style2 spotlights" id="one">
				<h1 class="welcome-text"><?php echo $en ? 'More infos': 'Plus d\'infos'; ?></h1>
				<section id="texte1">
					<a class="image" href="#"><img alt="" data-position="center center" src="images/plage.jpg"/></a>
					<div class="content">
						<div class="inner">
							<?php include 'assets/texts/' . ($en ? 'en' : 'fr') . '/kesako.html'; ?>
						</div>
					</div>
				</section>
				<section id="texte2">
					<a href="#" class="image"><img src="images/Trajet.png" alt="" data-position="center center" /></a>
					<div class="content">
						<div class="inner">
							<?php include 'assets/texts/' . ($en ? 'en' : 'fr') . '/roadmap.html'; ?>
						</div>
					</div>
				</section>
				<section id="texte3">
					<a href="#" class="image"><img src="images/Activités WEI.jpeg" alt="" data-position="center center" /></a>
					<div class="content">
						<div class="inner">
							<?php include 'assets/texts/' . ($en ? 'en' : 'fr') . '/planning.html'; ?>
						</div>
					</div>
				</section>
				<section id="texte4">
					<a href="#" class="image"><img src="images/A amener.jpeg" alt="" data-position="25% 25%" /></a>
					<div class="content">
						<div class="inner">
							<?php include 'assets/texts/' . ($en ? 'en' : 'fr') . '/checklist.html'; ?>
							<?php showmore('texte4'); ?>
						</div>
					</div>
				</section>
				<section id="texte5">
					<a href="#" class="image"><img src="images/Point bizutage.jpeg" alt="" data-position="25% 25%" /></a>
					<div class="content">
						<div class="inner">
							<?php include 'assets/texts/' . ($en ? 'en' : 'fr') . '/rec.html'; ?>
							<?php showmore('texte5'); ?>
						</div>
					</div>
				</section>
			</section>

			<!-- Two -->


			<section class="floating-section wrapper style2 spotlights" id="two">
				<h1 class="welcome-text"><?php echo $en ? 'Integration week': 'Semaine d\'intégration'; ?></h1>

				<section>
					<a class="image" href="#"><img alt="" data-position="25% 25%" src="images/semaine_inte/3.jpg"/></a>
					<div class="content" style="width: 100%;">
						<div class="inner">
							<?php include 'assets/texts/' . ($en ? 'en' : 'fr') . '/patio.html'; ?>
						</div>
					</div>
				</section>

				<section>
					<a class="image" href="#"><img alt="" data-position="25% 25%" src="images/semaine_inte/1.jpg"/></a>
					<div class="content" style="width: 100%;">
						<div class="inner">
							<?php include 'assets/texts/' . ($en ? 'en' : 'fr') . '/prewei.html'; ?>
						</div>
					</div>
				</section>

				<section>
					<a class="image" href="#"><img alt="" data-position="25% 25%" src="images/semaine_inte/2.jpg"/></a>
					<div class="content" style="width: 100%;">
						<div class="inner">
						<?php include 'assets/texts/' . ($en ? 'en' : 'fr') . '/pot.html'; ?>
						</div>
					</div>
				</section>
			</section>


			<a href="internationaux.html">
				<section class="floating-section wrapper fade-up" id="three">

					<h1 class="welcome-text"><?php echo $en ? 'Internationals': 'Internationaux'; ?></h1>


					<section style="flex-direction: column;">

						<div class="content" style="width: 100%">
							<div class="inner" style="width: 100%">
								<div class="more longText" style="width: 100%">
									<div id="image-internationaux"></div>
									<div class="extra-padding" style="display: flex; flex-direction: column">
									<?php include 'assets/texts/' . ($_GET['lang'] == 'en' ? 'en' : 'fr') . '/internationals-short.html'; ?>
										<a class="button" href="internationaux.php<?php if($_GET['lang'] == 'en') echo '?lang=en' ?>"><?php echo ($_GET['lang'] == 'en' ? 'More informations' : 'Plus d\'informations'); ?></a>
									</div>
								</div>
							</div>
						</div>

					</section>


				</section>
			</a>

			<section class="floating-section wrapper fade-up" id="four">
				<h1 class="welcome-text">Contact</h1>

				<section class="extra-padding">
					<div class="content">
						<div class="inner">
							<div class="more longText">
								<?php include 'assets/texts/' . ($en ? 'en' : 'fr') . '/contact.html'; ?>
									<img onclick="overlay_show('images/trombi.jpg')" src="images/trombi.jpg" id="trombi">
								<section>
									<h3><?php
									if($en)
										echo 'To be always up to date on the latest news, follow us on our social networks';
									else
									    echo 'Pour toujours être à jour sur les dernières nouveautés, suis nous sur nos réseaux sociaux';
									?></h3>
									<div id="logo-container">
										<a class="media-logo-filler"></a>
										<a class="media-logo" href="https://www.facebook.com/bde.telecomparis"></a>
										<a class="media-logo" href="https://www.instagram.com/bde_telecomparis/"></a>
										<a class="media-logo" href="https://www.linkedin.com/company/bde-t%C3%A9l%C3%A9com-paristech/"></a>
										<a class="media-logo-filler"></a>
									</div>

								</section>
							</div>
						</div>
					</div>
				</section>


			</section>


			<section class="floating-section wrapper fade-up" id="five">
				<h1 class="welcome-text">FAQ - <?php echo $en ? 'Frequently asked questions': 'Questions fréquentes'; ?> </h1>
				<section class="extra-padding">
					<div class="content">
						<div class="inner">
							<div class="more longText">
								<p><?php echo $en ? 'No questions asked yet !': 'Pas encore de questions !'; ?></p>
							</div>
						</div>
					</div>
				</section>
			</section>

			<section class="floating-section wrapper fade-up" id="six">
				<h1 class="welcome-text"><?php echo $en ? 'Ticketing': 'Billetterie'; ?></h1>
				<section class="extra-padding">
					<div class="content">
						<div class="inner">
							<div class="more longText">
							<?php include 'assets/texts/' . ($en ? 'en' : 'fr') . '/ticketing.html'; ?>
							</div>
						</div>
					</div>
				</section>
			</section>

		</div>

	<!-- Footer -->
	<footer class="wrapper" id="footer">
		<div class="inner">
			<ul class="menu">
				<li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
			</ul>
		</div>
	</footer>

	<!-- Stylesheets -->
	<noscript>
		<link href="assets/css/noscript.css" rel="stylesheet"/>
	</noscript>
	<link href="assets/css/main.css" rel="stylesheet"/>
	<link href="style.css" rel="stylesheet"/>
	<link href="carousel.css" rel="stylesheet"/>
	<link href="mobile.css" rel="stylesheet"/>
    <link href="overlay.css" rel="stylesheet">

	<!-- Scripts -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery.scrollex.min.js"></script>
	<script src="assets/js/jquery.scrolly.min.js"></script>
	<script src="assets/js/browser.min.js"></script>
	<script src="assets/js/breakpoints.min.js"></script>
	<script src="assets/js/util.js"></script>
	<script src="assets/js/main.js"></script>
	<script src="script.js"></script>
    <script src="overlay.js"></script>

	</body>
</html>