const DEBUG = !!localStorage.getItem("debug");

//localStorage.setItem("debug",true);

function afficher_plus(id) {
    const elem = document.getElementById(id);
    const p = elem.getElementsByClassName('more')[0]
    const a = elem.getElementsByTagName('a')[1]
    if (p.className === 'more') // Afficher plus
    {
        p.className = "more longText";
        a.innerText = "Afficher moins"
    } else // Afficher moins
    {
        p.className = "more";
        a.innerText = "Afficher plus"
    }

}

function magie_checkbox(ul)
{
    let i = 0;
    let html ="<form id='myformID' class='form-control'>";
    for(let li of  ul.children) {
        const texte = li.innerHTML;
        html += `<input type="checkbox" name="checkbox-magique-${i}" id="checkbox-magique-${i}">
                    <label for="checkbox-magique-${i}" >${texte}</label>
                    <br/>`;
        i++;
    }
    html += "</form>"
    ul.outerHTML = html;
//
}

function onload_magie_checkbox() {
    magie_checkbox(document.getElementById("liste-magique"))

    reloadSaved(); //re-load any saved data
    $('.form-control').bind("change", function () {
        SaveForm();   //Save the form
    });

}


//https://github.com/acbrandao/Javascript/blob/master/JavascriptSnippets/js_save_load_form.html
/** ****************************************************************************************** **/
/** Permet de sauvegarder la liste dans le localStorage */
function SaveForm() {
    $.each( $('#myformID :input'), function() {
        const input_name = $(this).attr('id');
        localStorage[input_name] = this.checked;
        //console.log("Saving: "+input_name +" = "+ this.checked ) ;
    });
}

function reloadSaved() {

    $.each($('form input'), function () {
        const input_name = $(this).attr('id');
        if (localStorage[input_name]) {
            if (localStorage[input_name] === "true") this.checked = true;
            //console.log("Restoring: "+input_name +" with "+localStorage[input_name]  ) ;
        }
    });
}

/** ****************************************************************************************** **/


/* Carousel */
function onload_carousel() {
    let items = [document.getElementById("item-1"), document.getElementById("item-2"), document.getElementById("item-3")];
    let images = document.getElementsByClassName("carousel-img")
    let item_checked = 1;
    let item_count = 0;
    let filenames = "1.JPG 14.JPG 15.JPG 16.JPG 18.JPG 2.JPG 21.JPG 22.JPG 24.JPG 25.JPG 26.JPG 27.JPG 29.JPG 3.JPG 30.JPG 31.JPG 32.JPG 35.JPG 38.JPG 4.JPG 41.JPG 42.JPG 43.JPG 44.JPG 45.JPG 49.JPG 5.JPG 50.JPG 51.JPG 52.JPG 55.JPG 57.JPG 59.JPG 6.JPG 60.JPG 63.JPG 64.JPG 66.JPG".split(" ")
    let item_max = filenames.length-1;

    document.getElementById("carousel-preload").innerHTML = `<img src="images/defile/${filenames[0]}"/>`;
    function triggerNextPhoto() {
        items[item_checked].checked = true;
        item_checked = (item_checked + 1) % 3;
        console.log(filenames[item_count + 1]);
        images[item_checked].src = `images/defile/${filenames[item_count + 1]}`
        item_count = (item_count + 1) % item_max;

        // preload next one
        document.getElementById("carousel-preload").innerHTML = `<img src="images/defile/${filenames[item_count+1]}"/>`;
        return false;
    }

    setInterval(() => {

        triggerNextPhoto();

    }, 3000);

}

/* loading */

let hasScrolled = false;
document.body.onscroll = (e) => {
    hasScrolled = true;
    document.body.onscroll = null;
}

function onload_loading_animation() {
    /*
        if (hasScrolled)
        {
            document.getElementById("loading").style.display = "none";
            document.body.style.overflow = "unset";

            return;
        }
        */

    setTimeout(function () {
        window.scrollTo(0, 0)
    }, 0);


    document.getElementById("loading").className += " loaded";
    setTimeout(() => {
        document.getElementById("loading").style.display = "none";
        document.body.style.overflow = "unset";


        setTimeout(function () {
            const a = document.getElementById("accueil");
            a.scrollIntoView({
                behavior: "smooth"
            });
        }, 2000);


    }, 2100);
}


/* onload */

function resize_image_internationaux() {
    let ratio = 403 / 1656;
    if (document.body.clientWidth < 950) // mobile
        ratio = 752 / 1656;
    $("#image-internationaux").height($("#image-internationaux").parent().width() * ratio)
}

window.onload = function () {
    onload_loading_animation();
    onload_carousel();
    onload_magie_checkbox();

    resize_image_internationaux()
    $(window).resize(resize_image_internationaux);

}

